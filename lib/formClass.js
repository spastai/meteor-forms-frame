/*******************************
 * Class for Forms
 * placed in subdirectory to be loaded first
 */
function Forms() {
	var models = {};
	var cruds = {};
	var fieldGenerators = [];
	var collections = {}; 	 
	var self = this;

	this.addGenerator = function(name, generator) {
		fieldGenerators[name] = generator;
	}
	this.getGenerators = function() {
		return fieldGenerators;
	}
	
	this.model = function(name, model, crud, collection) {
		models[name] = model;
		if(crud) {
			cruds[name] = crud;
		}
		if(collection) {
			collections[name] = collection;
		}
	};
	
	this.getForm = function(name) {
		return models[name];
	};

	this.getCrud = function(name) {
		return cruds[name];
	};

	this.getCollection = function(name) {
		return collections[name];
	};

	// deprecated: use getCrud(..).remove();
	this.remove = function(name, id) {
		cruds[name].remove(id);
	};

	this.getValues = function(name,template) {
		var result = {};
		var form = this.getForm(name);
		for (var propName in form) {
			var type = form[propName].type;
			var element = template.find('#'+name+" #"+propName);
			if(fieldGenerators[type]) {
				if(fieldGenerators[type].getValue) {
					result[propName] = fieldGenerators[type].getValue($(element), propName); 
					//v("Getting custom value:"+propName+":"+result[propName]);
				}
			} 
			if(undefined == result[propName]) {
				// jquery is required for multi select
				// TODO template should be scoping but it is not 
				result[propName] = $(element).val();
				//v("Getting value:"+propName+":"+result[propName]);
			}
		}
		return result;
	};
	
};

forms = new Forms();
