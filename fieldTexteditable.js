forms.addGenerator("editable", {
  generateField: function(params) {
    var sfield = params.field;
    var sform =  'dictationForm';
    var def = CollectionDefaults.findOne({form:sform, field:sfield});
    if (def !== undefined) {
        params.value = CollectionDefaults.findOne({form:sform, field:sfield}).list[0] || "  ";
    } else {

    }
    return Template['fieldEditable'](params);
  },
  getValue: function(element) {
    return $(element).text();
  }
});

Template.fieldEditable.rendered = function() {
  var sfield = this.data.field;
  var sform =  'dictationForm';
  var scolldef = CollectionDefaults.findOne({'field':sfield});
  // Take care of placeholders if exists;
  if (this.data.definition.placeholder !== undefined) {
      $('#'+this.data.field).contentEditable({
        "placeholder" : this.data.definition.placeholder,
        "newLineOnEnterKey" : false
      });
  }

  // Take care of Autocomplete from Mongo Collection "scolldef" if exists
  if (scolldef !== undefined) {
     $('#'+this.data.field).autocomplete({
          source: function(query, process) {
          var suggestions = scolldef.list || ["List undefined"];
          process(suggestions);
         },
        minLength: 0
     }).focus(function(){
      $(this).data("autocomplete").search($(this).val());
    });
  } else {
  	// console.log ("Field dropdown defaults", sfield, " is undefined")
  }
};


