/*******************************
 * ListEdit component keeps lsit in memory
 */

Template.feildListedit.items = function() {
	return Session.get('#'+this.field+'-list');
};

Template.feildListedit.events({
	'click .addToList': function (event, template) {
		var item = $(template.find('#'+this.field)).val();
		//v("Save item:"+item);
		var list = Session.get('#'+this.field+'-list');
		if(!list) {
			list = [];
		}
		list.push(item);
		Session.set('#'+this.field+'-list', list);
		$(template.find('#'+this.field)).val('')
	},
	'click .listEditItem': function (event, template) {
		//d("Item clicked:"+this+" for:",template.data);
		$(template.find('#'+template.data.field)).val(this);
	},
	'click .deleteFromList': function (event, template) {
		var item = $(template.find('#'+this.field)).val();		
		var list = Session.get('#'+this.field+'-list');
		Session.set('#'+this.field+'-list', _.without(list, item));
	}
});

forms.addGenerator('listedit', {
 	generateField: function(params) {
 		//d("Listedit:",params);
 		var propName = this.field;
		Session.set('#'+propName+'-list', this.value);
		return Template['feildListedit'](params);
	},
	
	getValue: function(node, propName) {
		return Session.get('#'+propName+'-list');
	}	
});
