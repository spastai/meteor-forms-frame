forms.addGenerator("tableedit", {
 	generateField: function(params) {	
		return Template['fieldTableedit'](params);
	},
	getValue: function(element) {
		return Session.get(this.field+'-table-edit') || [];;		
	}
});

Template.fieldTableedit.rows = function() {
	var list = Session.get(this.field+'-table-edit');
	//d("Rows:",list);
	return list;
}

Template.fieldTableedit.column = function() {
	var row = this;
	return _.chain(this)
			.keys()
			.map(function(key) { return row[key]; }) 
			.value();
}

Template.fieldTableedit.events({
	'click .addRow': function (event, template) {
		var values = forms.getValues(this.definition.form, template);
		//d("Values:"+this.definition.form,values);
		var list = Session.get(this.field+'-table-edit') || [];
		list.push(values)
		Session.set(this.field+'-table-edit', list);
	},
});