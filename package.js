Package.describe({
  summary: 'Meteor form framework'
});

Package.on_use(function (api) {

    api.use(['underscore', 'templating'], 'client');
    api.add_files([
         'lib/formClass.js',
         'lib/models.js',
         'fieldCheckbox.html',
         'fieldCheckboxGroup.html',
         'fieldDatepicker.html',
         'fieldListEdit.html',
         'fieldMultivalue.html',
         'fieldPassword.html',
         'fieldSelect.html',
         'fieldString.html',
         'fieldSubform.html',
         'fieldTableEdit.html',
         'fieldTextarea.html',
         'fieldTexteditable.html',
         'formControllBare.html',
         'formControllBareSmall.html',
         'formControllSmall.html',
         'forms.html',
         'fieldCheckbox.js',
         'fieldCheckboxGroup.js',
         'fieldDatepicker.js',
         'fieldListEdit.js',
         'fieldMultivalue.js',
         'fieldSelect.js',
         'fieldString.js',
         'fieldSubform.js',
         'fieldTableEdit.js',
         'fieldTexteditable.js',
         'forms.js'],
          'client');
  });
