Template.fieldCheckboxGroup.items = function() {
	var result = [];
	var options = this.definition.options();
	for(o in options){
		var selected = options[o].value == this.value ? "checked" : "";
		result.push({value:options[o].value, selected: selected, title:options[o].title});
	};
	return result;
};
