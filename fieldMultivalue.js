// Not able to access parent context, thus pushing items here
Template.fieldMultivalue.fields = function() {
	var self = this;
	var result =  _.chain(_.zip(this.definition.fields, this.definition.placeholders))
		.map(function(item) { 
			item.push(self.value ? self.value[item[0]] : "");
			var field = _.object(["field", "placeholder", "value"],item);
			field.class = self.definition.class;
			return field;
		})
		.value();
	return result;
};
